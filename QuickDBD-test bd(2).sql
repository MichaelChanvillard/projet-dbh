CREATE TABLE `cart` (
    `id` INT AUTO_INCREMENT NOT NULL ,
    `user_id` INT  DEFAUlT NULL ,
    `product_id` int  DEFAULT NULL ,
    `color_id` int  DEFAULT NULL ,
    `date` VARCHAR(256)  DEFAULT NULL,
    PRIMARY KEY (
        `id`
    )
);

CREATE TABLE `user` (
    `id` INT AUTO_INCREMENT NOT NULL ,
    `first_name` VARCHAR(256)  DEFAULT NULL ,
    `second_name` VARCHAR(256)  DEFAULT NULL ,
    `contact_id` INT DEFAULT NULL ,
    PRIMARY KEY (
        `id`
    )
);

CREATE TABLE `contact` (
    `id` INT AUTO_INCREMENT NOT NULL ,
    `phone` VARCHAR(256) DEFAULT NULL ,
    `mail` VARCHAR(256)  DEFAULT NULL ,
    `address_id` INT  DEFAULT NULL ,
    PRIMARY KEY (
        `id`
    )
);

CREATE TABLE `address` (
    `id` INT AUTO_INCREMENT NOT NULL ,
    `address` VARCHAR(256)  DEFAULT NULL ,
    `city` VARCHAR(256)  DEFAULT NULL ,
    `contry` VARCHAR(256)  DEFAULT NULL ,
    PRIMARY KEY (
        `id`
    )
);

CREATE TABLE `product` (
    `id` INT AUTO_INCREMENT NOT NULL ,
    `name_product` VARCHAR(256)  DEFAULT NULL ,
    `description_product` TEXT  DEFAULT NULL ,
    `price_product` VARCHAR(256) DEFAULT NULL ,
    `WWW` VARCHAR(256)  DEFAULT NULL ,
    `keyword_id` INT  DEFAULT NULL ,
    `provider_id` INT  DEFAULT NULL ,
    PRIMARY KEY (
        `id`
    )
);

CREATE TABLE `color` (
    `id` INT AUTO_INCREMENT NOT NULL ,
    `color` VARCHAR(256)  DEFAULT NULL,
    PRIMARY KEY (
        `id`
    )
);

CREATE TABLE `keyword` (
    `id` INT AUTO_INCREMENT NOT NULL ,
    `the_keyword` VARCHAR(256)  DEFAULT NULL ,
    PRIMARY KEY (
        `id`
    )
);

CREATE TABLE `provider` (
    `id` INT AUTO_INCREMENT NOT NULL ,
    `the_provider` VARCHAR(256)  DEFAULT NULL ,
    PRIMARY KEY (
        `id`
    )
);

ALTER TABLE `cart` ADD CONSTRAINT `fk_cart_user_id` FOREIGN KEY(`user_id`)
REFERENCES `user` (`id`);

ALTER TABLE `cart` ADD CONSTRAINT `fk_cart_product_id` FOREIGN KEY(`product_id`)
REFERENCES `product` (`id`);

ALTER TABLE `cart` ADD CONSTRAINT `fk_cart_color_id` FOREIGN KEY(`color_id`)
REFERENCES `color` (`id`);

ALTER TABLE `user` ADD CONSTRAINT `fk_user_contact_id` FOREIGN KEY(`contact_id`)
REFERENCES `contact` (`id`);

ALTER TABLE `contact` ADD CONSTRAINT `fk_contact_address_id` FOREIGN KEY(`address_id`)
REFERENCES `address` (`id`);

ALTER TABLE `product` ADD CONSTRAINT `fk_product_keyword_id` FOREIGN KEY(`keyword_id`)
REFERENCES `keyword` (`id`);

ALTER TABLE `product` ADD CONSTRAINT `fk_product_provider_id` FOREIGN KEY(`provider_id`)
REFERENCES `provider` (`id`);

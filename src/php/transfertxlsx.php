<?php
//namespace Src\php;
use \PDO;
//use Src\php\conect_database;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
$row = [];
$target_file = "./src/xlsx/database18.xlsx";
$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();

    $db_host = 'localhost';
    $db_name = 'projetphp' ;
    $db_user = 'root';
    $db_pass = 'root';
    $dbh = new PDO('mysql:host='.$db_host.';dbname='.$db_name,$db_user,$db_pass,[
      PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, //error
      PDO:: ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ // objet
    ]);



if($reader) {
  $reader->setReadDataOnly(true);
  $spreadsheet = $reader->load($target_file);  
  $sheetData = $spreadsheet->getSheetByName("Clients")->toArray();

  foreach($sheetData as $row) {
    // get columns
    $id_address = isset($row[0]) ? $row[0] : "";
    $address = isset($row[6]) ? $row[6] : "";
    $city = isset($row[5]) ? $row[5] : "";
    $contry = isset($row[7]) ? $row[7] : "";

    // insert item
    $query = "INSERT INTO address (id,address,city,contry) ";
    $query .= "values(?, ?, ?, ?)";
    $prep = $dbh->prepare($query);
    $prep->execute(array($id_address, $address, $city, $contry));
  }
}

if($reader) {
    $reader->setReadDataOnly(true);
    $spreadsheet = $reader->load($target_file);  
    $sheetData = $spreadsheet->getSheetByName("Clients")->toArray();
  
    foreach($sheetData as $row) {
      // get columns
      $id_contact = isset($row[0]) ? $row[0] : "";
      $phone = isset($row[4]) ? $row[4] : "";
      $mail = isset($row[3]) ? $row[3] : "";

      // insert item
      $query = "INSERT INTO contact(id, phone, mail, address_id)";
      $query .= "values(?, ?, ?, ?)";
      $prep = $dbh->prepare($query);
      $prep->execute(array($id_contact, $phone, $mail, $id_contact));
    }
  }

  if($reader) {
    $reader->setReadDataOnly(true);
    $spreadsheet = $reader->load($target_file);  
    $sheetData = $spreadsheet->getSheetByName("Clients")->toArray();
  
    foreach($sheetData as $row) {
      // get columns
      $id_user = isset($row[0]) ? $row[0] : "";
      $first_name = isset($row[1]) ? $row[1] : "";
      $second_name = isset($row[2]) ? $row[2] : "";

      // insert item
      $query = "INSERT INTO user(id, first_name, second_name, contact_id)";
      $query .= "values(?, ?, ?, ?)";
      $prep = $dbh->prepare($query);
      $prep->execute(array($id_user, $first_name, $second_name, $id_user));

    }
  }

    if($reader) {
    
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($target_file);  
        $sheetData = $spreadsheet->getSheetByName("Produits")->toArray();
  
    foreach($sheetData as $row) {
      // get columns
      $id_keyword = isset($row[0]) ? $row[0] : "";
      $keyword = isset($row[5]) ? $row[5] : "";
      
      // insert item
      $query = "INSERT INTO keyword(id, the_keyword)";
      $query .= "values(?, ?)";
      $prep = $dbh->prepare($query);
      $prep->execute(array($id_keyword, $keyword));
    };
  };

  if($reader) {

    $reader->setReadDataOnly(true);
    $spreadsheet = $reader->load($target_file);  
    $sheetData = $spreadsheet->getSheetByName("Produits")->toArray();
    
      
        foreach($sheetData as $row) {
          // get columns
          $id_provider = isset($row[0]) ? $row[0] : "";
          $provider = isset($row[2]) ? $row[2] : "";
          
          // insert item
          $query = "INSERT INTO provider(id, the_provider)";
          $query .= "values(?, ?)";
          $prep = $dbh->prepare($query);
          $prep->execute(array($id_provider, $provider));
        };

      };
    
    //////////////////////////////////////table COLOR///////////////////////////////////////////////////////
    
    
      if($reader) {
        
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($target_file);  
        $sheetData = $spreadsheet->getSheetByName("Ventes")->toArray();
      
        foreach($sheetData as $row) {
          // get columns
          $id_color = isset($row[0]) ? $row[0] : "";
          $color = isset($row[4]) ? $row[4] : "";
          
          // insert item
          $query = "INSERT INTO color(id, color)";
          $query .= "values(?, ?)";
          $prep = $dbh->prepare($query);
          $prep->execute(array($id_color, $color));
        };
        $latest_id_color = $dbh->lastInsertId();
      };
    
    //////////////////////////////////////table PRODUCT///////////////////////////////////////////////////////
    

    
      if($reader) {
    
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($target_file);  
        $sheetData = $spreadsheet->getSheetByName("Produits")->toArray();
      
        foreach($sheetData as $row) {
          // get columns
          $id_product = isset($row[0]) ? $row[0] : "";
          $name_product = isset($row[1]) ? $row[1] : "";
          $description_product = isset($row[3]) ? $row[3] : "";
          $price_product  = isset($row[6]) ? $row[6] : "";
          $www = isset($row[4]) ? $row[4] : "";
      
          // insert item
          $query = "INSERT INTO product(id, name_product, description_product, price_product , www , keyword_id, provider_id)";
          $query .= "values(?, ?, ?, ?, ?, ?, ?)";
          $prep = $dbh->prepare($query);
          $prep->execute(array($id_product, $name_product, $description_product, $price_product, $www, $id_product, $id_product));
          
        };

      };
    
    
    //////////////////////////////////////table CART///////////////////////////////////////////////////////
    
    
      if($reader) {
        
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($target_file);  
        $sheetData = $spreadsheet->getSheetByName("Ventes")->toArray();
      
        foreach($sheetData as $row) {
          // get columns
          $id_cart = isset($row[0]) ? $row[0] : "";
          $id_user = isset($row[1]) ? $row[1] : "";
          $id_user=intval(trim($id_user,"customer-"));
          $is_product = isset($row[2]) ? $row[2] : "";
          $id_product=intval(trim($id_product,"product-")); 
          $date  = isset($row[3]) ? $row[3] : "";
      
      
          // insert item
          $query = "INSERT INTO cart(id, user_id, product_id, color_id, date )";
          $query .= "values(?, ?, ?, ?, ?)";
          $prep = $dbh->prepare($query);
          $prep->execute(array($id_cart, $id_user, $id_product, $id_cart, $date));
        };
      };


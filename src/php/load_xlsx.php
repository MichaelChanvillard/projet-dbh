<?php
namespace Src\php;
use \PDO;
use Src\php\conect_database;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;

class conect_database{

  public $dbh;

  public function __construct(){
    $db_host = 'localhost';
    $db_name = 'projetphp' ;
    $db_user = 'root';
    $db_pass = 'root';
    $this->dbh = new PDO('mysql:host='.$db_host.';dbname='.$db_name,$db_user,$db_pass,[
      PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, //error
      PDO:: ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ // objet
    ]);
  }
}
$row = [];
class load_xlsx{


  public function load_xlsx(){

    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
    $target_file = "./src/xlsx/database18.xlsx";
    $reader->setReadDataOnly(true);
    $spreadsheet = $reader->load($target_file); 
    return $spreadsheet;

  }

  public function reader_xlsx_clients($name){

    $sheetData = $this->load_xlsx()->getSheetByName($name)->toArray();
    return $sheetData;
  }

  public function reader_xlsx_Ventes(){

    $sheetData = $this->load_xlsx()->getSheetByName("Ventes")->toArray();
    return $sheetData;
  }

  public function reader_xlsx_Produits(){

    $sheetData = $this->load_xlsx()->getSheetByName("Produits")->toArray();
    return $sheetData;
  }

  public function table_address(){
  
      $sheetData = $this->reader_xlsx_clients();
  
      foreach($sheetData as $row) {
    
        // get columns
        $id_address = isset($row[0]) ? $row[0] : "";
        $address = isset($row[5]) ? $row[5] : "";
        $city = isset($row[6]) ? $row[6] : "";
        $contry = isset($row[7]) ? $row[7] : "";
    
        // insert item
        $query = "INSERT INTO address (id,address,city,contry) ";
        $query .= "values(?, ?, ?, ?)";
        $prep = $dbh->prepare($query);
        $prep->execute(array($id_address, $address, $city, $contry()));
        $prep->closeCursor();
  
      }
      //$latest_id_address = $this->dbh->lastInsertId();
      //return $latest_id_address;
  }

  public function table_contact(){

    $sheetData = $this->reader_xlsx_clients();
    
      foreach($sheetData as $row) {
        // get columns
        $id_contact = isset($row[0]) ? $row[0] : "";
        $phone = isset($row[4]) ? $row[4] : "";
        $mail = isset($row[3]) ? $row[3] : "";
        
    
        // insert item
        $query = "INSERT INTO contact(id, phone, mail, address_id)";
        $query .= "values(?, ?, ?, ?)";
        $prep = $dbh->prepare($query);
        $prep->execute(array($id_contact, $phone, $mail, $this->table_address()));
      };
      $latest_id_contact = $dbh->lastInsertId();
    }
  
    public function table_user(){

        $this->reader_xlsx_clients();
      
        foreach($sheetData as $row) {
          // get columns
          $id_user = isset($row[0]) ? $row[0] : "";
          $first_name = isset($row[1]) ? $row[1] : "";
          $second_name = isset($row[2]) ? $row[2] : "";
          
      
          // insert item
          $query = "INSERT INTO user(id, first_name, second_name, contact_id)";
          $query .= "values(?, ?, ?, ?)";
          $prep = $dbh->prepare($query);
          $prep->execute(array($id_user, $first_name, $second_name, $this->table_contact()));
        };
        $latest_id_user = $dbh->lastInsertId();
    }
      
};






// //////////////////////////////////////table KEYWORD///////////////////////////////////////////////////////

// function table_keyword(){

//   if($reader) {
    
//     reader_xlsx_Produits();
  
//     foreach($sheetData as $row) {
//       // get columns
//       $id_keyword = isset($row[0]) ? $row[0] : "";
//       $keyword = isset($row[5]) ? $row[5] : "";
      
//       // insert item
//       $query = "INSERT INTO keyword(id, keyword)";
//       $query .= "values(?, ?)";
//       $prep = $dbh->prepare($query);
//       $prep->execute(array($id_keyword, $keyword));
//     };
//     $latest_id_keyword = $dbh->lastInsertId();
//   };
// };

// //////////////////////////////////////table PROVIDER///////////////////////////////////////////////////////

// function table_provider(){

//   if($reader) {
    
//     reader_xlsx_Produits();
  
//     foreach($sheetData as $row) {
//       // get columns
//       $id_provider = isset($row[0]) ? $row[0] : "";
//       $provider = isset($row[2]) ? $row[2] : "";
      
//       // insert item
//       $query = "INSERT INTO provider(id, provider)";
//       $query .= "values(?, ?)";
//       $prep = $dbh->prepare($query);
//       $prep->execute(array($id_provider, $provider));
//     };
//     $latest_id_provider = $dbh->lastInsertId();
//   };
// };

// //////////////////////////////////////table COLOR///////////////////////////////////////////////////////

// function tables_color(){

//   if($reader) {
    
//     reader_xlsx_Vents();
  
//     foreach($sheetData as $row) {
//       // get columns
//       $id_color = isset($row[0]) ? $row[0] : "";
//       $color = isset($row[4]) ? $row[4] : "";
      
//       // insert item
//       $query = "INSERT INTO color(id, color)";
//       $query .= "values(?, ?)";
//       $prep = $dbh->prepare($query);
//       $prep->execute(array($id_color, $color));
//     };
//     $latest_id_color = $dbh->lastInsertId();
//   };
// };

// //////////////////////////////////////table PRODUCT///////////////////////////////////////////////////////

// function table_product(){

//   if($reader) {

//     reader_xlsx_Produits();
  
//     foreach($sheetData as $row) {
//       // get columns
//       $id_product = isset($row[0]) ? $row[0] : "";
//       $name_product = isset($row[1]) ? $row[1] : "";
//       $description_product = isset($row[3]) ? $row[3] : "";
//       $price_product  = isset($row[6]) ? $row[6] : "";
//       $www = isset($row[4]) ? $row[4] : "";
  
//       // insert item
//       $query = "INSERT INTO product(id, name_product, description_product, price_product , www , color_id, keyword_id, provider_id)";
//       $query .= "values(?, ?, ?, ?, ?, ?, ?, ?)";
//       $prep = $dbh->prepare($query);
//       $prep->execute(array($id_product, $name_product, $description_product, $price_product, $www, $last_id_color, $last_id_keyword, $last_id_provider));
//     };
//     $latest_id_product = $dbh->lastInsertId();
//   };
// };


// //////////////////////////////////////table CART///////////////////////////////////////////////////////

// function table_cart(){

//   if($reader) {
    
//     reader_xlsx_Vents();
  
//     foreach($sheetData as $row) {
//       // get columns
//       $id_cart = isset($row[0]) ? $row[0] : "";
//       $customer = isset($row[1]) ? $row[1] : "";
//       $product = isset($row[2]) ? $row[2] : "";
//       $date  = isset($row[3]) ? $row[3] : "";
  
  
//       // insert item
//       $query = "INSERT INTO cart(id, customer, user_id, product, product_id, date )";
//       $query .= "values(?, ?, ?, ?, ?, ?)";
//       $prep = $dbh->prepare($query);
//       $prep->execute(array($id_cart, $customer, $last_id_user, $product, $last_id_product, $date));
//     };
//   };
// };










?>


<?php

$error = null;
try {
        $product = $dbh->query('SELECT product.name_product, product.description_product, 
        product.price_product, product.WWW, keyword.the_keyword, provider.the_provider 
        FROM product
        LEFT JOIN keyword On product.keyword_id = keyword.id
        LEFT JOIN provider On product.provider_id = provider.id 
        ');
        
        $vues_product = $product->fetchAll(); 


}catch(PDOException $e){
    $error = $e->getMessage();
};

?>

<?php if ($error): ?>
    <div class="alert allert-danger"><?=$error?></div>
<?php else: ?>
    <table class="table table-responsive table-bordered table-striped table-dark">
    <thead>
    <tr>
      <th scope="col">Produits</th>
      <th scope="col">Descrition</th>
      <th scope="col">prix</th>
      <th scope="col">WWW</th>
      <th scope="col">Mots clefs</th>
      <th scope="col">Fourniseur</th>
    </tr> 
  </thead>
  <tbody>
    <?php foreach($vues_product as $vues_product): ?>
   
    <tr>
    <td><?= $vues_product->name_product ?></td>
    <td><?= $vues_product->description_product  ?></td>
    <td><?= $vues_product->price_product ?></td>
    <td><?= $vues_product->WWW ?></td>
    <td><?= $vues_product->the_keyword?></td>
    <td><?= $vues_product->the_provider ?></td>
    </tr>
    <?php endforeach ?>
  </tbody>
  </table>
    






<?php endif ?>
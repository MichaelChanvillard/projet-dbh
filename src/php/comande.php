<?php

$error = null;
try {

    $cart = $dbh->query('SELECT user.first_name, user.second_name, contact.phone,
contact.mail, address.address, address.city, address.contry,
product.name_product, product.description_product,
product.price_product, product.WWW, keyword.the_keyword, 
provider.the_provider, color.color, cart.date 

FROM cart
LEFT JOIN (user LEFT JOIN (contact LEFT JOIN address on address_id = address.id)ON contact_id=contact.id)ON user_id=user.id
LEFT JOIN (product LEFT JOIN keyword On product.keyword_id = keyword.id
LEFT JOIN provider On product.provider_id = provider.id)ON product_id = product.id
LEFT JOIN color ON color_id = color.id');

$vues_cart =  $cart->fetchAll();
   

}catch(PDOException $e){
    $error = $e->getMessage();
}
?>

<?php if ($error): ?>
    <div class="alert allert-danger"><?=$error?></div>
<?php else: ?>
    <table class="table table-dark">
    <thead>
    <tr>
      <th scope="col">Nom</th>
      <th scope="col">Prenom</th>
      <th scope="col">Téléphone</th>
      <th scope="col">Mail</th>
      <th scope="col">Adresse</th>
      <th scope="col">Villes</th>
      <th scope="col">Pays</th>
      <th scope="col">Produits</th>
      <th scope="col">Descrition</th>
      <th scope="col">prix</th>
      <th scope="col">WWW</th>
      <th scope="col">Mots clefs</th>
      <th scope="col">Fourniseur</th>
      <th scope="col">couleur</th>
      <th scope="col">date</th>
      
    </tr> 
  </thead>
  <tbody>
    <?php foreach($vues_cart as $vues_cart): ?>
   
    <tr>
    <td><?= $vues_cart->first_name ?></td>
    <td><?= $vues_cart->second_name ?></td>
    <td><?= $vues_cart->phone ?></td>
    <td><?= $vues_cart->mail ?></td>
    <td><?= $vues_cart->address?></td>
    <td><?= $vues_cart->city ?></td>
    <td><?= $vues_cart->contry ?></td>
    <td><?= $vues_cart->name_product ?></td>
    <td><?= $vues_cart->description_product  ?></td>
    <td><?= $vues_cart->price_product ?></td>
    <td><?= $vues_cart->WWW ?></td>
    <td><?= $vues_cart->the_keyword?></td>
    <td><?= $vues_cart->the_provider ?></td>
    <td><?= $vues_cart->color ?></td>
    <td><?= $vues_cart->date ?></td>
    </tr>
    <?php endforeach ?>
  </tbody>
  </table>
    
<?php endif ?>
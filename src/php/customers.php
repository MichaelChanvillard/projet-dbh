<?php

$error = null;
try {
    $user = $dbh->query('SELECT user.first_name, user.second_name, contact.phone,
    contact.mail, address.address, address.city, address.contry
    FROM user
    LEFT JOIN (contact LEFT JOIN address on address_id=address.id) ON contact_id=contact.id
');
    $vues_user =  $user->fetchAll();

}catch(PDOException $e){
    $error = $e->getMessage();
}
?>

<?php if ($error): ?>
    <div class="alert allert-danger"><?=$error?></div>
<?php else: ?>
    <table class="table table-dark">
    <thead>
    <tr>
      <th scope="col">Nom</th>
      <th scope="col">Prenom</th>
      <th scope="col">Téléphone</th>
      <th scope="col">Mail</th>
      <th scope="col">Adresse</th>
      <th scope="col">Villes</th>
      <th scope="col">Pays</th>
    </tr> 
  </thead>
  <tbody>
    <?php foreach($vues_user as $vues_user): ?>
   
    <tr>
    <td><?= $vues_user->first_name ?></td>
    <td><?= $vues_user->second_name ?></td>
    <td><?= $vues_user->phone ?></td>
    <td><?= $vues_user->mail ?></td>
    <td><?= $vues_user->address?></td>
    <td><?= $vues_user->city ?></td>
    <td><?= $vues_user->contry ?></td>
    </tr>
    <?php endforeach ?>
  </tbody>
  </table>
    
<?php endif ?>